#include <algorithm>
#include <vector>
#include <cstdio>
#include <queue>

using namespace std;

class Compare {
public: 
    bool operator() (int x, int y) {
        return x < y;
    }
};

int main() {
    vector < int > v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);

    sort(v.begin(), v.end(), Compare());
    
    printf("Sort: ");
    for (int i = 0; i < v.size(); ++i)
        printf("%d ", v[i]);
    printf("\n");

    priority_queue < int, vector < int >, Compare > P;
    
    P.push(1);
    P.push(2);
    P.push(3);
    
    printf("Heap: ");
    while (!P.empty()) {
        printf("%d ", P.top());
        P.pop();
    }
    printf("\n");

    return 0;
}
