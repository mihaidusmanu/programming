#include <cstdio>
#include <stack>

using namespace std;

int main() {
    freopen("skyline.in", "r", stdin);
    freopen("skyline.out", "w", stdout);

    int N;
    scanf("%d", &N);

    stack < pair < int, int > > S;
    long long ans = -1;

    for (int i = 0; i < N; ++i) {
        int height, length;
        scanf("%d%d", &height, &length);

        int currentHeight, currentLength = 0;
        while (!S.empty() && S.top().first > height) {
            currentHeight = S.top().first;
            currentLength += S.top().second;
            S.pop();            

            if (1LL * currentHeight * currentLength > ans)
                ans = 1LL * currentHeight * currentLength;
        }

        if (!S.empty() && S.top().first == height)
            S.top().second += currentLength + length;
        else {
            S.push(make_pair(height, currentLength + length));
        }
    }

    int currentHeight, currentLength = 0;
    while (!S.empty()) {
        currentHeight = S.top().first;
        currentLength += S.top().second;
        S.pop();

        if (1LL * currentHeight * currentLength > ans)
            ans = 1LL * currentHeight * currentLength;
    }

    printf("%lld", ans);

    return 0;
}
