#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

const int kMaxL = 400;

const long long INF = (1LL << 63) - 1;

long long getMin(long long m1, long long M1, long long m2, long long M2, char sgn) {
    if (sgn == '+')
        return m1 + m2;
    if (sgn == '*')
        return min(m1 * m2, min(m1 * M2, min(M1 * m2, M1 * M2)));
    return min(m1 - m2, min(M1 - m2, min(m1 - M2, M1 - M2)));
}

long long getMax(long long m1, long long M1, long long m2, long long M2, char sgn) {
    if (sgn == '+')
        return M1 + M2;
    if (sgn == '*')
        return max(m1 * m2, max(m1 * M2, max(M1 * m2, M1 * M2)));
    return M1 - m2;
}

int main() {
    #ifndef ONLINE_JUDGE
    freopen("test.in", "r", stdin);
    #endif

    int k;
    while (scanf("%d", &k) != EOF) {
        vector < pair < long long, char > > elem;
        
        for (int i = 0; i < k; ++i) {
            int x;
            char ch;
            scanf("%d %c", &x, &ch);
            elem.push_back(make_pair(x, ch));
        }

        for (int i = 0; i < k; ++i)
            elem.push_back(elem[i]);

        long long m[kMaxL][kMaxL], M[kMaxL][kMaxL];

        for (int i = 0; i < kMaxL; ++i)
            for (int j = 0; j < kMaxL; ++j) {
                m[i][j] = INF;
                M[i][j] = -INF;
            }

        for (int i = 0; i < elem.size(); ++i)
            m[i][i] = M[i][i] = elem[i].first;

        for (int dif = 1; dif < elem.size(); ++dif)
            for (int i = 0; i < elem.size() - dif; ++i) {
                int j = i + dif;
                for (int k = i; k < j; ++k)
                    if (elem[k].second == '?') {
                        m[i][j] = min(m[i][j], getMin(m[i][k], M[i][k], m[k + 1][j], M[k + 1][j], '*'));
                        m[i][j] = min(m[i][j], getMin(m[i][k], M[i][k], m[k + 1][j], M[k + 1][j], '+'));
                        m[i][j] = min(m[i][j], getMin(m[i][k], M[i][k], m[k + 1][j], M[k + 1][j], '-'));
                        M[i][j] = max(M[i][j], getMax(m[i][k], M[i][k], m[k + 1][j], M[k + 1][j], '*'));
                        M[i][j] = max(M[i][j], getMax(m[i][k], M[i][k], m[k + 1][j], M[k + 1][j], '+'));
                        M[i][j] = max(M[i][j], getMax(m[i][k], M[i][k], m[k + 1][j], M[k + 1][j], '-'));
                    } else {
                    	m[i][j] = min(m[i][j], getMin(m[i][k], M[i][k], m[k + 1][j], M[k + 1][j], elem[k].second));
                    	M[i][j] = max(M[i][j], getMax(m[i][k], M[i][k], m[k + 1][j], M[k + 1][j], elem[k].second));
                    }
            }

        for (int i = 0; i < k; ++i)
            printf("%lld%lld", abs(m[i][i + k - 1]), abs(M[i][i + k - 1]));
        printf("\n");
    }

    return 0;
}
