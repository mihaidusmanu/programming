#include <vector>
#include <cstdio>
#include <queue>

using namespace std;

const int kMaxN = 10000;
const int INF = 0x3f3f3f3f;

vector < pair <int, int> > adj[kMaxN];

int P, T, dist[kMaxN], sol[kMaxN];

void init(int v[], int n, int defaultValue) {
    for (int i = 0; i < n; ++i)
        v[i] = defaultValue;
}

int answer(int cn) {
    if (sol[cn] != -1)
        return 0;

    int ans = 0;

    for (int i = 0; i < adj[cn].size(); ++i)
        if (dist[adj[cn][i].first] + adj[cn][i].second == dist[cn])
            ans += answer(adj[cn][i].first) + 2 * adj[cn][i].second;

    sol[cn] = ans;
    return ans;
}

int solve() {
    priority_queue < pair <int, int>, vector < pair <int, int> >, greater < pair <int, int> > > T;
    
    init(dist, P, INF);
    
    dist[0] = 0;
    T.push(make_pair(0, 0));

    while (!T.empty()) {
        pair <int, int> ec = T.top();
        T.pop();
        int cn = ec.second, cw = ec.first;

        for (int i = 0; i < adj[cn].size(); ++i)
            if (dist[adj[cn][i].first] > dist[cn] + adj[cn][i].second) {
                dist[adj[cn][i].first] = dist[cn] + adj[cn][i].second;
                T.push(make_pair(dist[adj[cn][i].first], adj[cn][i].first));
            }
    }
    
    init(sol, P, -1);
    return answer(P - 1);
}

void clear() {
    for (int i = 0; i < P; ++i)
        adj[i].clear();
}

int main() {
    //freopen("test.in", "r", stdin);
    
    while (scanf("%d%d", &P, &T) != EOF) {
        for (int i = 0; i < T; ++i) {
            int x, y, l;
            scanf("%d%d%d", &x, &y, &l);
            adj[x].push_back(make_pair(y, l));
            adj[y].push_back(make_pair(x, l));
        }

        printf("%d\n", solve());
    
        clear();
    }

    return 0;
}
