#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;

const int kMaxN = 1000;

vector < int > randomVect(int N) {
    vector < int > v;

    for (int i = 0; i < N; ++i)
        v.push_back(rand() % 2);

    return v;
}

//X and Y must have the same size
bool isEqual(vector < int > X, vector < int > Y) {
    for (int i = 0; i < X.size(); ++i)
        if (X[i] != Y[i])
            return false;
    return true;
}

vector < int > mmv(vector < vector < int > > a, vector < int > v) {
    vector < int > ans;

    for (int i = 0; i < a.size(); ++i) {
        ans.push_back(0);
        for (int j = 0; j < a[i].size(); ++j)
            ans[i] += a[i][j] * v[j];
    }

    return ans;
}

bool verif(vector < vector < int > > a, vector < vector < int > > b) {
    vector < int > v = randomVect(a.size());
    return isEqual(mmv(a, mmv(a, v)), mmv(b, v));
}

int main() {
    #ifndef ONLINE_JUDGE
    freopen("test.in", "r", stdin);
    #endif
    
    srand(time(NULL));

    int N;
    scanf("%d", &N);

    while (N != 0) {
        vector < vector < int > > a;
        for (int i = 0; i < N; ++i) {
            vector < int > v;
            for (int j = 0; j < N; ++j) {
                int x;
                scanf("%d", &x);
                v.push_back(x);
            }
            a.push_back(v);
        }
        
        vector < vector < int > > b;
        for (int i = 0; i < N; ++i) {
            vector < int > v;
            for (int j = 0; j < N; ++j) {
                int x;
                scanf("%d", &x);
                v.push_back(x);
            }
            b.push_back(v);
        }

        if (verif(a, b) && verif(a, b) && verif(a, b))
            printf("YES\n");
        else
            printf("NO\n");
        
        scanf("%d", &N);
    }

    return 0;
}
