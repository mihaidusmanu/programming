#include <cstdio>
#include <math.h>

int max(int x, int y) {
    return x < y ? y : x;
}

int main() {
    #ifndef ONLINE_JUDGE
    freopen("test.in", "r", stdin);
    freopen("test.out", "w", stdout);
    #endif

    int T;
    scanf("%d", &T);

    for (int i = 0; i < T; ++i) {
        int x;
        scanf("%d", &x);
        
        if (x <= 2) {
            printf("IMPOSSIBLE\n");
            continue;
        }

        int l2 = 2;
        while (l2 <= x) {
            if (x % l2 == l2 / 2)
                break;
            l2 *= 2;
        }

        int aux = x;
        while (aux % 2 == 0)
            aux /= 2;
        int lpf = 1;
        int s = sqrt(x) + 1;
        for (int i = 3; i <= s; ++i)
            if (aux % i == 0) {
                lpf = i;
                break;
            }
        if (lpf == 1)
            lpf = aux;
    
        if (1 < lpf && lpf < l2) {
            if (x / lpf - (lpf - 1) / 2 >= 0) {
                printf("%d = ", x);
                for (int i = max(x / lpf - (lpf - 1) / 2, 1); i < x / lpf + (lpf - 1) / 2; ++i)
                    printf("%d + ", i);
                printf("%d\n", x / lpf + (lpf - 1) / 2);
            } else 
                printf("IMPOSSIBLE\n");
        } else {
            if ((x - l2 / 2) / l2 - (l2 / 2 - 1) >= 0) {
                printf("%d = ", x);
                for (int i = max((x - l2 / 2) / l2 - (l2 / 2 - 1), 1); i < (x - l2 / 2) / l2 + (l2 / 2); ++i)
                    printf("%d + ", i);
                printf("%d\n", (x - l2 / 2) / l2 + (l2 / 2));
            } else
                printf("IMPOSSIBLE\n");
        }
    }
        
    
    return 0;
}
