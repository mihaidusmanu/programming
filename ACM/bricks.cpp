#include <cstdio>
#include <vector>

using namespace std;

int ind(char ch) {
    if (ch == 'B') 
        return 1;
    return 0;
}

int euclid(int x, int y) {
    if (y == 0) 
        return x;
    return euclid(y, x % y);
}

int main() {
    #ifndef ONLINE_JUDGE
    freopen("test.in", "r", stdin);
    #endif

    int Q;
    scanf("%d", &Q);

    for (int i = 0; i < Q; ++i) {
        vector < pair < int, int > > v;

        int s[2];
        s[0] = s[1] = 0;
        
        int N;
        scanf("%d", &N);
        for (int i = 0; i < N; ++i) {
            int l;
            char ch;
            scanf("%d %c", &l, &ch);
            int ord = ind(ch);
            v.push_back(make_pair(l, ord));
            s[ord] += l;
        }
        
        int aux = euclid(s[0], s[1]);
        int rat[2];
        rat[0] = s[0] / aux;
        rat[1] = s[1] / aux;
        
        if (rat[0] == 0) {
            printf("%d\n", s[1]);
            continue;
        }

        if (rat[1] == 0) {
            printf("%d\n", s[0]);
            continue;
        }
    
        int n[2], ans = 0;
        n[0] = n[1] = 0;
        
        n[v[0].second] = v[0].first;
        for (int i = 1; i < v.size(); ++i) {
            if (n[1 - v[i].second] % rat[1 - v[i].second] == 0) {
                int req = (n[1 - v[i].second] / rat[1 - v[i].second]) * rat[v[i].second] - n[v[i].second];
                if (0 < req && req <= v[i].first) {
                    ++ans;
                    n[0] = n[1] = 0;
                    n[v[i].second] = v[i].first - req;
                } else
                    n[v[i].second] += v[i].first;
            } else 
                n[v[i].second] += v[i].first; 
        }
       
        printf("%d\n", ans);
    }
     

    return 0;
}
