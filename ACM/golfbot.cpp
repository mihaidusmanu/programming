#include <cstdio>
#include <complex>
#include <vector>
#include <algorithm>
#include <math.h>

using namespace std;

const int L = 1 << 19;

const complex < double > unit (1.0, 0.0);

const complex < double > zero (0.0, 0.0);

const double eps = 0.000001;

vector < complex < double > > FFT(vector < complex < double > > A, complex < double >  w) {
    vector < complex < double > > F; 
    
    if (A.size() == 1) {
        F.push_back(A[0]);
        return F;
    }

    vector < complex < double > > A_even, A_odd;
    for (int i = 0; i < A.size(); ++i) 
        if (i % 2 == 0)
            A_even.push_back(A[i]);
        else
            A_odd.push_back(A[i]);

    vector < complex < double > > F_even = FFT(A_even, w * w), F_odd = FFT(A_odd, w * w);

    complex < double > x (unit);
    for (int i = 0; i < A.size() / 2; ++i) {
        F.push_back(F_even[i] + x * F_odd[i]);
        x = x * w;
    }

    x = unit;
    for (int i = 0; i < A.size() / 2; ++i) {
        F.push_back(F_even[i] - x * F_odd[i]);
        x = x * w;
    }

    return F;
}

vector < complex < double > > SQUARE(vector < complex < double > > A) {
    complex < double > w (cos(2.0 * M_PI / double(A.size())), sin(2.0 * M_PI / double(A.size())));
    vector < complex < double > > F_A = FFT(A, w), F_C;
    
    for (int i = 0; i < F_A.size(); ++i)
        F_C.push_back(F_A[i] * F_A[i]);
    
    vector < complex < double > > C = FFT(F_C, unit / w);
    
    for (int i = 0; i < C.size(); ++i)
        C[i] /= double(C.size());

    return C;
}

int main() {
    //freopen("test.in", "r", stdin);
    
    int N;

    while (scanf("%d", &N) != EOF) {
        vector <int> dist;
        for (int i = 0; i < N; ++i) {
            int x;
            scanf("%d", &x);
            dist.push_back(x);
        }
        sort(dist.begin(), dist.end());
    
        vector < complex < double > > A;
        int LC = int(pow(2.0, ceil(log2(2 * dist[N - 1]))));
        for (int i = 0; i < LC; ++i)
            A.push_back(zero);
        for (int i = 0; i < dist.size(); ++i)
            A[dist[i]] = unit;

        vector < complex < double > > R = SQUARE(A);
        
        int M, ans = 0;
        scanf("%d", &M);
        for (int i = 0; i < M; ++i) {
            int x;
            scanf("%d", &x);

            if (x < LC && (norm(R[x]) > eps || norm(A[x]) > eps))
                ++ans;
        }

        printf("%d\n", ans);
    }


    return 0;
}
