#include <cstdio>
#include <algorithm>
#include <math.h>

using namespace std;

const int kMaxN = 1000;
const double eps = 0.000001;

bool test(int n, double M, double w) {
    pair <double, double> inter[kMaxN];

    for (int i = 0; i < n; ++i) {
        double pc;
        scanf("%lf", &pc);
        inter[i].first = max(pc - w / 2, 0.0);
        inter[i].second = min(pc + w / 2, M);
    }

    sort(inter, inter + n);

    double lend = 0.0;

    int posc = 0;

    while (posc < n && inter[posc].first <= lend + eps) {
        lend = inter[posc].second;
        posc += 1;
    }

    return (fabs(M - lend) < eps);
}

int main() {
    int nx, ny;
    double w;

    scanf("%d%d%lf", &nx, &ny, &w);

    while (nx != 0 && ny != 0 && w > eps) {
        if (test(nx, 75.0, w) && test(ny, 100.0, w))
            printf("YES\n");
        else
            printf("NO\n");

        scanf("%d%d%lf", &nx, &ny, &w);
    }

    return 0;
}
