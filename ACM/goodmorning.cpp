#include <cstdio>

using namespace std;

const int kMaxN = 201;

int m[4][3];

bool mark[kMaxN];

void df(int nb, int i, int j) {
    if (nb * 10 + m[i][j] > 200)
        return;
    mark[nb * 10 + m[i][j]] = true;
    for (int ix = 0; ix < 4; ++ix)
        for (int iy = 0; iy < 3; ++iy)
            if (i + ix < 4 && j + iy < 3 && m[i + ix][j + iy] != -1)
                df(nb * 10 + m[i][j], i + ix, j + iy);
}

int search(int x) {
    int nb = 0;

    for (int i = x; i >= 0; --i)
        if (mark[i]) {
            nb = i;
            break;
        }
    
    for (int i = x; i <= 200; ++i)
        if (mark[i]) {
            if (i - x <= x - nb)
                nb = i;
            break;
        }
    
    return nb;
}

int main() {
    #ifndef ONLINE_JUDGE
    freopen("test.in", "r", stdin);
    #endif
    
    int k = 0;
    for (int i = 0; i < 3; ++i)
        for (int j = 0; j < 3; ++j)
            m[i][j] = ++k;
    m[3][0] = -1;
    m[3][1] = 0;
    m[3][2] = -1;
    
    mark[0] = true;

    for (int i = 0 ; i < 3; ++i)
        for (int j = 0 ; j < 3; ++j)
            df(0, i, j);
    
    /*for (int i = 0; i <= 200; ++i)
        printf("%d -> %d\n", i, mark[i]);
    printf("\n");*/

    int Q;
    scanf("%d", &Q);
    for (int i = 0; i < Q; ++i) {
        int x;
        scanf("%d", &x);
        printf("%d\n", search(x));
    }

    return 0;
}
