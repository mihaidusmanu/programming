#include <cstdio>
#include <string>
#include <vector>

using namespace std;

const int kMaxA = 26, kMaxL = 200;

const long long INF = (1LL << 50);

int getPos(char x, char v[], int N) {
    for (int i = 0; i < N; ++i)
        if (x == v[i])
            return i;
}

vector < int > transform(string s, char v[], int N) {
    vector < int > t;
    for (int i = 0; i < s.size(); ++i)
        t.push_back(getPos(s[i], v, N));
    return t;
}

pair < long long, int > compute(vector < int > v, int cost[kMaxA][kMaxA], int result[kMaxA][kMaxA], int N) {
    long long din[v.size()][v.size()][kMaxA];

    for (int i = 0; i < v.size(); ++i)
        for (int j = 0; j < v.size(); ++j)
            for (int k = 0; k < N; ++k)
                din[i][j][k] = INF;

    for (int i = 0 ; i < v.size(); ++i)
        din[i][i][v[i]] = 0;

    for (int dif = 1; dif < v.size(); ++dif)
        for (int i = 0; i < v.size() - dif; ++i) {
            int j = i + dif;
            for (int k = i; k < j; ++k)
                for (int a = 0; a < N; ++a)
                    for (int b = 0; b < N; ++b)
                        if (din[i][k][a] != INF && din[k + 1][j][b] != INF)
                            din[i][j][result[a][b]] = min(din[i][j][result[a][b]], din[i][k][a] + cost[a][b] + din[k + 1][j][b]);
        }
    
    long long ans = INF;
    int ch = 0;
    for (int a = 0; a < N; ++a)
        if (ans > din[0][v.size() - 1][a]) {
            ans = din[0][v.size() - 1][a];
            ch = a;
        }

    return make_pair(ans, ch);
}

int main() {
    #ifndef ONLINE_JUDGE
    freopen("test.in", "r", stdin);
    #endif

    int alpha;
    scanf("%d\n", &alpha);
    while (alpha != 0) {
        char types[kMaxA];
        for (int i = 0; i < alpha; ++i)
            scanf("%c ", &types[i]);

        int cost[kMaxA][kMaxA], result[kMaxA][kMaxA];
        for (int i = 0; i < alpha; ++i)
            for (int j = 0; j < alpha; ++j) {
                char aux;
                scanf("%d-%c", &cost[i][j], &aux);
                result[i][j] = getPos(aux, types, alpha);
            }

        int q;
        scanf("%d\n", &q);
        for (int i = 0; i < q; ++i) {
            char s[kMaxL];
            scanf("%s", s);
            string str (s);

            pair < long long, int > p = compute(transform(str, types, alpha), cost, result, alpha);
            printf("%lld-%c\n", p.first, types[p.second]);
        }

        scanf("%d\n", &alpha);
        if (alpha)
            printf("\n");
    }

    return 0;
}
