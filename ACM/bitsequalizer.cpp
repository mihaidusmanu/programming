#include <cstdio>
#include <vector>
#include <string>

using namespace std;

const int kMaxL = 100;

int solve(string A, string B) {
    vector < int > w0, w1, qm0, qm1;
    for (int i = 0; i < A.size(); ++i)
        if (A[i] != B[i]) {
            if (A[i] == '0')
                w0.push_back(i);
            else if (A[i] == '1')
                w1.push_back(i);
            else if (B[i] == '0')
                qm0.push_back(i);
            else if (B[i] == '1')
                qm1.push_back(i);
        }

    int n0 = 0, n1 = 0;
    for (int i = 0; i < B.size(); ++i)
        if (B[i] != A[i]) {
            if (B[i] == '0')
                ++n0;
            else
                ++n1;
        }

    int op = qm0.size() + qm1.size();

    while (!w0.empty() && !w1.empty()) {
        w0.pop_back();
        w1.pop_back();
        --n0;
        --n1;
        ++op;
    }

    while (!w0.empty()) {
        w0.pop_back();
        --n1;
        ++op;
    }

    if (w1.size() > n1)
        return -1;

    while (!w1.empty() && !qm1.empty()) {
        w1.pop_back();
        qm1.pop_back();
        --n0;
        --n1;
        ++op;
    }

    return op;
}

int main() {
    #ifndef ONLINE_JUDGE
    freopen("test.in","r", stdin);
    freopen("test.out", "w", stdout);
    #endif

    int Q;
    scanf("%d\n", &Q);

    for (int i = 1; i <= Q; ++i) {
        char a[kMaxL], b[kMaxL];
        scanf("%s\n%s\n", a, b);
        string A (a);
        string B (b);
        
        printf("Case %d: %d\n", i, solve(A, B));
    }

    return 0;
}
