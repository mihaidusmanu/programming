#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

const int kMaxN = 10, kMaxL = 10;

int n;

char str[kMaxN][kMaxL];

vector <int> config;

vector <char> letters;

bool used[10];

bool isLetter(char x) {
    if (int(x) >= 'A' && int(x) <= 'Z')
        return true;
    return false;
}

bool check(char x, vector <char> l) {
    for (int i = 0; i < l.size(); ++i)
        if (l[i] == x)
            return true;

    return false;
}

void getLetters() {
    letters.clear();
    for (int i = 0; i < n; ++i)
        for (int j = 0; isLetter(str[i][j]); ++j)
            if (!check(str[i][j], letters))
                letters.push_back(str[i][j]);
}

int intOfChar(char x) {
    return (int(x) - int('A'));
}

void init(int v[], int n) {
    for (int i = 0; i < n; ++i)
        v[i] = -1;
}

bool test() {
    int v[26];

    init(v, 26);

    for (int i = 0; i < config.size(); ++i)
        v[intOfChar(letters[i])] = config[i];

    int ans = 0;

    for (int i = 0; i < n - 1; ++i) {
        if (v[intOfChar(str[i][0])] == 0)
            return false;

        int nc = 0;

        for (int j = 0; isLetter(str[i][j]); ++j)
            nc = nc * 10 + v[intOfChar(str[i][j])];

        ans += nc;
    }

    if (v[intOfChar(str[n - 1][0])] == 0)
        return false;

    int nc = 0;

    for (int j = 0; isLetter(str[n - 1][j]); ++j)
        nc = nc * 10 + v[intOfChar(str[n - 1][j])];

    return (ans == nc);
}

int solve() {
    if (config.size() >= letters.size()) {
        if (test())
            return 1;
        return 0;
    }

    int ans = 0;

    for (int i = 0; i <= 9; ++i)
        if (!used[i]) {
            config.push_back(i);
            used[i] = true;
            ans += solve();
            used[i] = false;
            config.pop_back();
        }

    return ans;
}

void reset() {
    for (int i = 0; i < kMaxN; ++i)
        for (int j = 0; j < kMaxL; ++j)
            str[i][j] = 0;
}

int main() {
    //freopen("test.in", "r", stdin);

    while (scanf("%d\n", &n) != EOF) {
        reset();

        for (int i = 0; i < n; ++i)
            scanf("%s", str[i]);

        getLetters();

        printf("%d\n", solve());
    }

    return 0;
}
