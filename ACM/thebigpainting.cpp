#include <algorithm>
#include <string>
#include <vector>
#include <queue>
#include <cstdio>
 
using namespace std;
 
const int kMaxL = 2000;
 
const int sigma = 2;
 
struct trieNode {
    int word;
    trieNode* child[sigma];
    trieNode* fail;
 
    trieNode() {
        word = -1;
        for (int i = 0; i < sigma; ++i)
            child[i] = NULL;
        fail = NULL;
    }
};
 
trieNode* R;
 
string mp;

vector < int > mpPattern;

int alpha(char c) {
    if (c == 'o')
        return 0;
    return 1;
}
 
void trieInsert(trieNode* N, string s, int pos, int code) {
    if (pos == s.length()) {
        N -> word = code;
        return;
    }
 
    if (N -> child[alpha(s[pos])] == NULL)
        N -> child[alpha(s[pos])] = new trieNode();
 
    trieInsert(N -> child[alpha(s[pos])], s, pos + 1, code);
}
 
vector < int > normalize(vector < string > v) {
    vector < string > vaux = v;
 
    sort(vaux.begin(), vaux.end());
    vector < string > :: iterator it = unique(vaux.begin(), vaux.end());
    vaux.resize(distance(vaux.begin(), it));
 
    for (int i = 0; i < vaux.size(); ++i)
        trieInsert(R, vaux[i], 0, i);
 
    vector < int > nor;
 
    for (int i = 0; i < v.size(); ++i)
        nor.push_back(distance(vaux.begin(), lower_bound(vaux.begin(), vaux.end(), v[i])));
 
    return nor;
}
 
void fails() {
    queue < trieNode* > Q;
 
    R -> fail = R;
    Q.push(R);
    while (!Q.empty()) {
        trieNode* cn;
 
        cn = Q.front();
        Q.pop();
 
        for (int i = 0; i < sigma; ++i)
            if (cn -> child[i] != NULL) {
                trieNode* f;
                f = cn -> fail;
                while (f -> child[i] == NULL && f != R)
                    f = f -> fail;
                if (f -> child[i] != NULL && f -> child[i] != cn -> child[i]) {
                    cn -> child[i] -> fail = f -> child[i];
                    if (cn -> child[i] -> word == -1)
                        cn -> child[i] -> word = f -> child[i] -> word;
                } else
                    cn -> child[i] -> fail = R;
                Q.push(cn -> child[i]);
            }
    }
}
 
void ahoCorasick() {
    trieNode* cn = R;
    int pos = 0;
    while (pos < mp.length()) {
        if (cn -> child[alpha(mp[pos])] == NULL) {
            if (cn == R) {
                mpPattern.push_back(-1);
                cn = R;
                ++pos;
            } else if (cn -> fail == NULL)
                cn = R;
            else
                cn = cn -> fail;
            continue;
        }
 
        mpPattern.push_back(cn -> child[alpha(mp[pos])] -> word);
        cn = cn -> child[alpha(mp[pos])];
        ++pos;
    }
}
 
vector < int > preKMP(vector < int > v) {
    vector < int > pi;
    int lc = 0;
    pi.push_back(0);
    pi.push_back(0);
 
    for (int i = 2; i < v.size(); ++i) {
        while (lc && v[i] != v[lc + 1])
            lc = pi[lc];
 
        if (v[i] == v[lc + 1])
            ++lc;
 
        pi.push_back(lc);
    }
 
    return pi;
}
 
int KMP(vector < int > pattern, vector < int > pi, vector < int > str) {
    int lc = 0;
    int ac = 0;
 
    for (int i = 1; i < str.size(); ++i) {
        while (lc && str[i] != pattern[lc + 1])
            lc = pi[lc];
 
        if (str[i] == pattern[lc + 1])
            ++lc;
 
        if (lc == pattern.size() - 1) { 
            ++ac;
            lc = pi[lc];
        }
    }
 
    return ac;
}
 
int main() {
    #ifndef ONLINE_JUDGE
    freopen("test.in", "r", stdin);
    #endif
 
    int hp, wp, hm, wm;
 
    while (scanf("%d%d%d%d", &hp, &wp, &hm, &wm) != EOF) {
        vector < string > painting;   
        mp.clear();
        mpPattern.clear();

        for (int i = 0; i < hp; ++i) {
            char str[kMaxL]; 
            scanf("%s", str);
            string s (str);
            painting.push_back(s);
        }

        for (int i = 0; i < hm; ++i) {
            char str[kMaxL];
            scanf("%s", str);
            string s (str);
            mp.insert(mp.end(), s.begin(), s.end());
        }
 
        R = new trieNode();
        vector < int > paintingPattern;
        paintingPattern.push_back(-1);
        vector < int > aux = normalize(painting);
        paintingPattern.insert(paintingPattern.end(), aux.begin(), aux.end());
        fails();

        ahoCorasick();
        
        vector < int > pi = preKMP(paintingPattern);
        
        int ans = 0;
        
        for (int j = wp - 1; j < wm; ++j) {
            vector < int > col;
            col.push_back(-1);
            for (int i = 0; i < hm; ++i)
                col.push_back(mpPattern[j + wm * i]);
            ans += KMP(paintingPattern, pi, col);
        }
 
        printf("%d\n", ans);
    }
 
    return 0;
}
