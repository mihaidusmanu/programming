#include <cstdio>
#include <vector>

using namespace std;

const int kMaxN = 10000;

int pairLR[kMaxN], pairRL[kMaxN];

bool vis[kMaxN];

bool match(vector <int> adj[], int nc) {
    if (vis[nc])
        return false;

    vis[nc] = true;

    for (int i = 0; i < adj[nc].size(); ++i)
        if (pairRL[adj[nc][i]] == -1) {
            pairLR[nc] = adj[nc][i];
            pairRL[adj[nc][i]] = nc;
            return true;
        }

    for (int i = 0; i < adj[nc].size(); ++i)
        if (match(adj, pairRL[adj[nc][i]])) {
            pairLR[nc] = adj[nc][i];
            pairRL[adj[nc][i]] = nc;
            return true;
        }
    
    return false;
}

int maxMatch(vector <int> adj[], int N) {
    for (int i = 0; i < N; ++i)
        pairLR[i] = pairRL[i] = -1;

    int value = 0;
    bool ok = true;
    while (ok) {
        ok = false;
        
        for (int i = 0; i < N; ++i)
            vis[i] = false;

        for (int i = 0; i < N; ++i)
            if (pairLR[i] == -1 && match(adj, i)) {
                ok = true;
                ++value;
            }
    }

    return value;
}

int main() {
    #ifndef ONLINE_JUDGE
    freopen("test.in", "r", stdin);
    #endif
    
    int N, M;

    while (scanf("%d%d", &N, &M) != EOF) {
        vector < int > adj[kMaxN];

        for (int i = 0; i < M; ++i) {
            int x, y;
            scanf("%d%d", &x, &y);
            adj[x].push_back(y);
        }

        if (maxMatch(adj, N) == N)
            printf("YES\n");
        else
            printf("NO\n");
    }

    return 0;
}
