#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int digit_of_char(char ch) {
  return (int)ch - (int)'0';
}

int main() {
  ifstream in;
  in.open("dataset.in");
  ofstream out;
  out.open("dataset.out");

  int T;
  in >> T;

  for (int i = 0; i < T; ++i) {
    int Smax = 0;
    string s;
    in >> Smax >> s;
    int nb[Smax + 1];
    int ans = 0;
    nb[0] = digit_of_char(s[0]);
    for (int j = 1; j <= Smax; ++j) {
      nb[j] = nb[j - 1] + digit_of_char(s[j]);
      if (nb[j - 1] + ans < j)
        ans += j - (nb[j - 1] + ans);
    }
    out << "Case #" << (i + 1) << ": " << ans << "\n";
  }

  in.close();
  out.close();
  return 0;
}
