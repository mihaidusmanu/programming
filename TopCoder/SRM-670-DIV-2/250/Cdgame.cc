
// {{{ VimCoder 0.3.6 <-----------------------------------------------------
// vim:filetype=cpp:foldmethod=marker:foldmarker={{{,}}}

#include <algorithm>
#include <bitset>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <utility>
#include <vector>

using namespace std;

// }}}

class Cdgame
{
public:
	int rescount(vector <int> a, vector <int> b)
	{
        int Sa = 0, Sb = 0;
        for (int i = 0; i < a.size(); ++i)
            Sa += a[i];
        for (int i = 0; i < b.size(); ++i)
		    Sb += b[i];
        
        set < int > s;
        for (int i = 0; i < a.size(); ++i)
            for (int j = 0; j < b.size(); ++j)
                s.insert((Sa - a[i] + b[j]) * (Sb - b[j] + a[i]));

        return int(s.size());
	}
};

